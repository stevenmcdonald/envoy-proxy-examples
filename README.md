Proxy setup for Envoy example apps
==================================

This document describes the setup for the proxy backed for [Envoy](https://github.com/greatfire/envoy) powered app. We'll cover the setup of the proxy for [our fork of the Wikipedia app](https://github.com/greatfire/apps-android-wikipedia-envoy), but the same general idea applies to the [other examples](https://github.com/greatfire/envoy/tree/master/apps) or other apps that might want to use Envoy.


## Example servers

All these examples can be run separately, or all on the same host or VM.

These [Ansible](https://www.ansible.com/) playbooks are tested on [Debian](https://www.debian.org/) 11.2, but should work on similar systems

### HTTP proxy: 

Envoy will send HTTP requests to the proxy with the target hostname in the `Host-Orig` header, and the target URL in the `Url-Orig`. Any proxy server that can rewrite the outgoing request based off those headers, and provide the required access control should work, we'll use [nginx](https://nginx.org/) in this example. 

[HTTP proxy example setup](./http_proxy/README.md)

### [Shadowsocks](https://shadowsocks.org/en/index.html):

This example uses the [shadowsocks-libev](https://github.com/shadowsocks/shadowsocks-libev) server

[Shadowsocks example setup](./shadowsocks/README.md)

### [Hysteria](https://github.com/HyNetwork/hysteria/wiki)

This example uses [Hysteria](https://github.com/HyNetwork/hysteria/wiki) [Github](https://github.com/HyNetwork/hysteria)

> Hysteria is a feature-packed proxy & relay utility optimized for lossy, unstable connections (e.g. satellite networks, congested public Wi-Fi, connecting from China to servers abroad) powered by a customized QUIC protocol.

[Hysteria example setup](./hysteria/README.md)

### Obfs4proxy with SOCKS

This example uses [obfs4proxy](https://gitlab.com/yawning/obfs4) with the [Dante](https://www.inet.no/dante/) SOCKS server.

This can also serve as the basis for tunnling other services though obfs4proxy, with minor modifications.

[Obfs4 + SOCKS example setup](./obfs4proxy_socks/README.md)

### Obfs4 tunneling HTTP using GOST

This example uses [GOST](https://v2.gost.run/en/) ([github](https://github.com/ginuerzh/gost/)) to provide a standard HTTP proxy tunneled though Obfs4.

GOST supports other protocols as well, such as Shadowsocks, and can combine them, e.g. Shadowsocks tunneled though Obfs4. This should be a good starting point if you want to use other protocols.

GOST also has access control options, unlike some of the other examples.

[GOST+Obfs4 http setup](./gost_obfs4_http/README.md)
