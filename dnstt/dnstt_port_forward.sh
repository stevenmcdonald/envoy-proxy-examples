#!/usr/bin/bash

# Forwawrd UDP packets for port 53 to port 5300 so dnstt doesn't have to
# run as root
# based on example here: https://www.bamsoftware.com/software/dnstt/

# this is not used in our example, instead we set:
# AmbientCapabilities=CAP_NET_BIND_SERVICE
# in the systemd unit file and listen on port 53 directly.

iptables -I INPUT -p udp --dport 5300 -j ACCEPT
iptables -t nat -I PREROUTING -i eth0 -p udp --dport 53 -j REDIRECT --to-ports 5300
ip6tables -I INPUT -p udp --dport 5300 -j ACCEPT
ip6tables -t nat -I PREROUTING -i eth0 -p udp --dport 53 -j REDIRECT --to-ports 5300
