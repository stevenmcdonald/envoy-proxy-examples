# Envoy Wikipedia Hysteria proxy

This is an example of a proxy setup for use with [Envoy](https://github.com/greatfire/envoy)

Other examples are [here](../README.md)

This example uses [Hysteria](https://github.com/HyNetwork/hysteria)

> Hysteria is a feature-packed proxy & relay utility optimized for lossy, unstable connections (e.g. satellite networks, congested public Wi-Fi, connecting from China to servers abroad) powered by a customized QUIC protocol.

## Configuration

Hysteria uses QUIC which requires a TLS certificate. Hysteria has built-in ACME support to request a certificate from [Let's Encrypt](https://letsencrypt.org/), but you need to be able to allow access to ports 80 and 443 for ACME challenges. We'll use a self-signed cert in this example, however consult the Hysteria docs if you'd like to use ACME. Altertatively, if you have genuine certs from another source, you can also use those in place of the self-signed certs, and you can remove the `ca` option from the client config.

The utility [mkcert](https://github.com/FiloSottile/mkcert) is helpful for creating self signed certs. Install mkcert somewhere, and run `mkcert -install` if this is your first time, to generate the local CA. Generate a cert for all the host names and IPs you wish to use, eg: `mkcert www.example.com 192.168.64.2`. Move `www.example.com+1.pem` to `hysteria.crt` and `www.example.com+1-key.pem` to hysteria.key in the directory of this playbook (you'll have diffent output names based on the domains or IPs you use)

You can also run `mkcert -CAROOT` to see the path to the CA files. You'll need the rootCA.pem file for the client.

Hysteria will listen on port 32323 for client connections, this can be changed in the playbook variables.

An example client configuration will be generated in `hysteria_dir` with the name `client-example.json` with values matching the server config.

The file `hysteria.acl` can be modified to set the server side ACL. See the [docs](https://github.com/HyNetwork/hysteria/wiki/ACL) for details. We limit connections to \*.wikipedia.org and \*.wikiemedia.org in this example. We also allow connections to www.google.com becuase Envoy uses a google.com URL to test the transport is working.

## Ansible

Add your target host to the `hysteria_servers` group in your inventroy (`inventory.ini` in this example):

```
[hysteria_servers]
192.168.64.2
```

If sudo on the target requires a password:

`ansible-playbook -i inventory.ini hysteria.yaml -K`

if not, you can exclude the `-K` option.

## Post Install

Verify the service is up and running `sudo journalctl -u hysteria -f`

To test:
* [Download](https://github.com/HyNetwork/hysteria/releases/tag/v1.0.5) the Hysteria binary for your platform, version 1.0.5 was the latest at the time of this writing. Copy the config file generated in `/usr/local/envoy/hysteria/client.json` and the RootCA.pem (from mkcert) on to your client. Make sure the `ca` setting points to the correct path for the RootCA.pem file, start up the client, it will listen for SOCKS5 connections on port 7080: `hysteria -c client.json`

Then we can test with curl: `curl -x sock5h://localhost:7080 https://www.wikipedia.org`
