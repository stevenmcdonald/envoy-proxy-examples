# Envoy Projct V/v2ray proxy setup

This is an example of a proxy setup for use with a [fork](https://github.com/greatfire/apps-android-wikipedia-envoy) of the Wikipedia app with [Envoy](https://github.com/greatfire/envoy)

Other examples are [here](../README.md)

This example uses v2ray from [Project V](https://www.v2fly.org/en_US/). The v2fly github account and v2fly.org seem to be the current source, though the (possibly older) documentation at https://www.v2ray.com/en/ seems more complete. This [Users' guide](https://guide.v2fly.org/en_US/) is fairly current, though incomplete. The a v2ray github seems to be older code. I'll refer to it simply as "v2ray", since that name still seems to be used for the client/server code we're using here.

There are also seem to be many other projects using forked versions of v2ray out there.

## Configuration

V2ray supports a number of transports, we'll document the setup of several options, but check the upstream documentation for other options.

### VMess Protocol:

> VMess protocol is originated from and utilised in V2ray, and it is similar with Shadowsocks which is designed for obfuscating internet traffic to cheat Deep packet inspection (opens new window)of GFW. VMess is the primary protocol used to communicate between server and client.

(docs from guide.v2fly.org)

According to [this tutorial](https://privacymelon.com/how-to-install-vless-xtls-xray-core/) plain vmess is easily detected by DPI, so we only use it under obfuscated transports.

### Transports:

We document examples of using a Websocket transport and 2 QUIC transports: masquerading as SRTP or Wechat video connections.

The websocket transport can be used with a CDN such as Cloudflare (we use Cloudflare in our demo setup), the QUIC based protocols need to connect directly since Clouldflre doesn't proxy UDP traffic. V2ray also supports an HTTP/2 transport. We don't document it here, but may in the future.

The [http proxy](../http_proxy/README.md) example has a block of nginx config to proxy websocket connections to V2ray (we'd prefer to keep the examples indpendent, but having them be compatible with each other wins out here). You'll also need to deploy that project, or set up a proxy based on the example there, to use websockets.

## Ansible

V2ray is packaged for Debian, so installation is pretty simple. We also install ntpd since accurate time is essential to v2ray's authentication. We also generate sample client configs for the supported transports.

Add your target host to the `v2ray_servers` group in your inventory file (`inventory.ini` in this example):

```
[v2ray_servers]
192.168.64.2
```

If sudo on the target requires a password:

`ansible-playbook -i inventory.ini hysteria.yaml -K`

if not, you can exclude the `-K` option.

## Post Install

Verify the service is up and running: `sudo service v2ray status`

To test:

[Download](https://github.com/v2fly/v2ray-core/releases/tag/v4.45.2) the v2ray software for you platform (or build from source). Copy the generated client configs from `/etc/v2ray/` and start up v2ray, e.g.: `v2ray -c client_srtp.json`

Then test with curl: `curl -x sock5h://localhost:9988 https://www.wikipedia.org/`

